import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div style={{position:'fixed',bottom:0,left:0,width:'100%', height:'70px', backgroundColor:'#ccc', textAlign:'center'}}>
          <h3>I am the footer component</h3>
      </div>
    )
  }
}
