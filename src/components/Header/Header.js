import React, { Component } from 'react'
import Avatar from "../Avatar/Avatar";
import './style.css';
export default class Header extends Component {
  render() {
    return (
      <div className="header">
        <h3 className="title">Page title</h3>
        <Avatar userName="Sai"/>
      </div>
    )
  }
}
