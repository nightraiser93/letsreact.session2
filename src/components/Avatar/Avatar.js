import React, { Component } from 'react'
import styles from "./Avatar.module.css";
export default class Avatar extends Component {
  constructor(props){
      super(props);
      const loadedTime = new Date();
      this.state = {loadedOn: loadedTime, elapsedTime: 0};
  }

   timeTicker = () => {
    const {loadedOn} = this.state;
    let elapsedTime = Math.abs(new Date().getTime() - loadedOn.getTime());
    elapsedTime = Math.floor(elapsedTime/1000);
    this.setState({elapsedTime})
  }
  componentDidMount(){
    setInterval(this.timeTicker, 1000);
  }
  render() {
      const userName = this.props.userName;
      const timeElapsed = this.state.elapsedTime;
    return (
      <div className={styles.avatar}>
       Hello <b>{userName}</b>
       <br /> loaded time  is {timeElapsed} seconds
      </div>
    )
  }
}
