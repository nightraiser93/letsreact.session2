import React, { Component } from 'react'
import styles from "./Sidebar.module.css";
export default class Sidebar extends Component {
  render() {
    return (
      <div className={styles.sidebar}>
          <p>I am sidebar component</p>
          <h3 className={styles.title}>Sidebar title</h3>
      </div>
    )
  }
}
